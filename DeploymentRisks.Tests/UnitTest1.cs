using Microsoft.VisualStudio.TestTools.UnitTesting;
using DeploymentRisks.Controllers;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;

namespace DeploymentRisksTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            HomeController hc = new HomeController(null);

            string someText = "Test Text";
            bool expected = true;

            bool result = hc.IsValidText(someText);

            Assert.AreEqual(expected, result);
        }
    }
}